package me.illuminatiproductions.cortexspawner;

import me.illuminatiproductions.cortexspawner.listeners.BlockBreakListener;
import me.illuminatiproductions.cortexspawner.listeners.CortexListeners;
import org.bukkit.plugin.java.JavaPlugin;

public final class CortexSpawner extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        getServer().getPluginManager().registerEvents(new BlockBreakListener(), this);
        getServer().getPluginManager().registerEvents(new CortexListeners(), this);
    }

}
